// Santosh  TEekulapally
// Student ID : C0738169

// Vishank Bhupeshkumar
// Student ID : C0712790

package com.example.droidcafewsettings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowToast;

import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;

@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }
 //test case one TC1:   Ordering a Frozen Yogurt
    @Test
    public void activityIsNotNull() throws Exception {
        ImageView button = (ImageView) activity.findViewById(R.id.froyo);
        button.performClick();
        assertThat(ShadowToast.getTextOfLatestToast(), equalTo("You ordered a FroYo.") );

    }

    //test case one TC2:   Cart page remembers user info

//    @Test
//
//    public void activityToCheckSavedInfoCustomer() throws Exception {
//
//
//        String input = etName.getText().toString();
//
//
//        EditText etName = (EditText) findViewById(R.id.name_text);
//        EditText etAddress = (EditText) findViewById(R.id.address_text);
//        EditText etPhone = (EditText) findViewById(R.id.phone_text);
//
//
//    }

}
